
const { exec } = require('child_process');
const serverPing    = require('../serverPing/serverPing');

function wait(seconds) {
  return new Promise((resolve) => {
    setTimeout(resolve, seconds * 1000);
  });
}

function runCommand() {
  return new Promise((resolve) => {
    exec('./start_pol', (err, stdout, stderr) => {
      if (stdout) console.log(stdout);
      if (stderr) console.log(stderr);
    }).unref();

    resolve();
  });
}

async function powerup() {
  exec('kilall -9 pol');
  await wait(1);

  while (1) {
    const status = await serverPing();
    if (status) return;
    console.log('Trying to Start POL');

    await runCommand();
    await wait(6);
  }
}
module.exports = powerup;

